﻿Imports System.Drawing.Imaging
Imports Microsoft.Win32

Public Class Main
    Dim WPaint As Boolean = False
    Dim WSav As String = My.Computer.FileSystem.SpecialDirectories.MyPictures & "\SCW_DesktopWallpaper.bmp"
    Private Declare Function SystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" (ByVal uAction As Integer, ByVal uParam As Integer, ByVal lpvParam As String, ByVal fuWinIni As Integer) As Integer
    Private Const SDWal = 20
    Private Const UIWal = &H1
    Private Sub WLoad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        WLbl.Parent = WBox
        WSel.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        WSel.Filter = "Graphics|*.JPG;*.PNG;*.BMP"
        Dim wallkey = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Control Panel\Desktop", "WallpaperStyle", True)
        Dim tilekey = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Control Panel\Desktop", "TileWallpaper", True)
        Select Case wallkey
            Case 0
                Select Case tilekey
                    Case 0
                        ComboBox1.SelectedItem = "Center"
                        Exit Select
                    Case 1
                        ComboBox1.SelectedItem = "Tile"
                        Exit Select
                End Select
                Exit Select
            Case 2
                ComboBox1.SelectedItem = "Stretch"
                Exit Select
            Case 6
                ComboBox1.SelectedItem = "Fit"
                Exit Select
            Case 10
                ComboBox1.SelectedItem = "Fill"
        End Select
    End Sub

    Private Sub WFileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles WSel.FileOk
        WBox.BackgroundImage = Image.FromFile(WSel.FileName)
    End Sub
    Private Sub PictureBox1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles WBox.Paint
        ControlPaint.DrawBorder(e.Graphics, Me.WBox.ClientRectangle, Color.Black, ButtonBorderStyle.Solid)
        If WPaint = True Then
            Dim semiTransBrush As New SolidBrush(Color.FromArgb(100, 0, 0, 0))
            e.Graphics.FillRectangle(semiTransBrush, WBox.Location.X - 3, WBox.Location.Y - 2, WBox.Width, WBox.Height)
            WLbl.Update()
        Else
            Dim semiTransBrush As New SolidBrush(Color.FromArgb(0, 0, 0, 0))
            e.Graphics.FillRectangle(semiTransBrush, WBox.Location.X - 3, WBox.Location.Y - 2, WBox.Width, WBox.Height)
        End If
    End Sub
    Private Sub WBox_MouseHover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WBox.MouseEnter, WLbl.MouseEnter
        WPaint = True
        WBox.Invalidate()
        WLbl.Text = "Click to set as wallpaper"
    End Sub
    Private Sub WBox_MouseHLeave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles WBox.MouseLeave, WLbl.MouseLeave
        WPaint = False
        WBox.Invalidate()
        WLbl.Text = ""
    End Sub
    Private Sub WBox_Click(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles WBox.Click, WLbl.Click
            If WBox.BackgroundImage Is Nothing Then
                MsgBox("Please select an image!", MsgBoxStyle.Exclamation)
            Else
                WBox.BackgroundImage.Save(WSav, ImageFormat.Bmp)
                Dim key As RegistryKey = Registry.CurrentUser.OpenSubKey("Control Panel\Desktop", True)
                Select Case ComboBox1.SelectedItem
                    Case "Tile"
                        key.SetValue("WallpaperStyle", "0")
                        key.SetValue("TileWallpaper", "1")
                        Exit Select
                    Case "Center"
                        key.SetValue("WallpaperStyle", "0")
                        key.SetValue("TileWallpaper", "0")
                        Exit Select
                    Case "Stretch"
                        key.SetValue("WallpaperStyle", "2")
                        key.SetValue("TileWallpaper", "0")
                        Exit Select
                    Case "Fit"
                        key.SetValue("WallpaperStyle", "6")
                        key.SetValue("TileWallpaper", "0")
                        Exit Select
                    Case "Fill"
                        key.SetValue("WallpaperStyle", "10")
                        key.SetValue("TileWallpaper", "0")
                        Exit Select
                End Select
                key.Close()
                SystemParametersInfo(SDWal, 0, WSav, UIWal)
            End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        WSel.ShowDialog()
    End Sub
End Class
